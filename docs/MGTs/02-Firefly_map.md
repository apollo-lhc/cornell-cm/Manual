# Firefly Naming Map

This document describes the naming convention for the Firefly modules. The Firefly modules can be identified by the FPGA they are connected to (F1 or F2) followed by a one-digit number. These correspond to quads on the FPGA.

The 12 channel slots can be either CERN-B (for detector-facing, lpGBT applications) or 25 Gbps for the backend/TF applications. The 4 channel slots are 25 Gbps only.

## Rev2

| Firefly Number | Type | Quad(s) | MCU mask bit(s) | EMP regions | 
| --- | --- | --- |  -- | --- |
| F1_1  | 12 ch | 121, 122, 123 | 0, 1 | 30, 29, 28 |
| F1_2  | 12 ch | 125, 126, 127 | 2, 3  | 26, 25, 24 |
| F1_3  | 12 ch | 132, 133, 134 | 4, 5 | 19, 18, 17 |
| F1_4  |  4 ch | 124 | 6 | 27 |
| F1_5  | 4 ch  | 128 | 7 | 23 |
| F1_6  | 4 ch  | 129 | 8 | 22 |
| F1_7  | 4 ch  | 130 | 9 | 21 |
| F2_1  | 12 ch | 121, 122, 123 | 10, 11 | 30, 29, 28 |
| F2_2  | 12 ch | 125, 126, 127 | 12, 13 | 26, 25, 24 |
| F2_3  | 12 ch | 132, 133, 134 | 14, 15 | 19, 18, 17 |
| F2_4  |  4 ch | 124 | 16  | 27 |
| F2_5  | 4 ch  | 128 | 17 | 23 |
| F2_6  | 4 ch  | 129 | 18 | 22 |
| F2_7  | 4 ch  | 130 | 19 | 21 |

This does not describe the quads that run between the FPGAs. F1 and F2 are identical for Firefly maps. The _bit(s)_ column refers to the entry in the internal MCU masks. In the case of two bits for one entry, it is split into separate Tx and Rx modules. 

EMP data is from the [EMP framework](https://gitlab.cern.ch/p2-xware/firmware/emp-fwk/-/blob/apollo_fpga2/boards/apollo/cm_v2/vu13p/firmware/hdl/emp_device_decl_cm_v2.vhd?ref_type=heads) (link to branch in emp-fwk repo).
