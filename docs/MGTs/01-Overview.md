# Overview of the Multigigabit transceivers on the Apollo CM


<img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/apollo_cm_mgts_rev3.png" alt="MGT Overview, Rev3" width="90%" >

This is an overview of the MGTs for the Rev3 Apollo CM. The following pages show the details of the Firefly and inter-FPGA MGTs.

Image below shows how the fireflies will be used in the Apollo CM for the IT-DTC and the TrackFinder.
<img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/apollo_cm_applications_rev3.png" alt="Firefly applications" width="90%" >
