# Inter-FPGA layout of MGTs

This document describes the layout of the MGTs in the Inter-FPGA communication.

## Rev 2

There are 52 inter-FPGA links between the two FPGAs in Rev 2.

The MGTs are laid out as follows. EMP F1 and F2 refer to the regon codes in the EMP framework. Note region 4 is missing on F1 and region 11 is missing on F2. See schematic page 1.03.

|SLR  |  FPGA1 Quad | FPGA2 Quad | EMP F1 | EMP F2 |
| --- | ----------- | ---------- | ------ | ------ |
| 0   | 234 J       | 221 M      | 14     | 1      |
| 0   | 233 I       | 222 N      | 13     | 2      |
| 0   | 232 H       | 223 O      | 12     | 3      |
| 1   | 231 G       | 224 P      | 11     | 4      |
| 1   | 230 F       | 225 A      | 10     | 5      |
| 1   | 229 E       | 226 B      |  9     | 6      |
| 1   | 228 D       | 227 C      |  8     | 7      |
| 2   | 227 C       | 228 D      |  7     | 8      |
| 2   | 226 B       | 229 E      |  6     | 9      |
| 2   | 225 A       | 230 F      |  5     | 10     |
| 3   | 223 O       | 232 H      |  3     | 12     |
| 3   | 222 N       | 233 I      |  2     | 13     |
| 3   | 221 M       | 234 J      |  1     | 14     |

### EMP connections

In EMP framework, the MGT quads are assigned to a number starting at 0 from each FPGA. For the inter-FPGA links, the MGT assignment is show in the table above, which is [derived from here](https://gitlab.cern.ch/p2-xware/firmware/emp-fwk/-/blob/master/boards/apollo/cm_v2/vu13p/firmware/hdl/emp_device_decl_cm_v2_p1.vhd?ref_type=heads) (link to branch in emp-fwk repo).
