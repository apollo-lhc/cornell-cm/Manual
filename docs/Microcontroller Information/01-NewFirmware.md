# Loading new firmware into the Microcontroller

There are two ways to load a new firmware onto the TM4C.  

1. If you have a SEGGER J-Link programmer connected, follow the [instructions](https://github.com/apollo-lhc/cm_mcu/wiki/Recovering-a-locked-TM4C#re-programming-via-jlinkexe) for reprogramming (ignore the locking part). If you are using Eclipse or some other debugger it might be as simple as starting a debugging session.
1. If you do not have a Segger (normally not installed on the board) the alternative is to use the uart to install a new binary via the boot loader. The executable is called `sflash` and can be built from the `mcu_tools` [GitHub repository](https://github.com/apollo-lhc/mcu_tools/tree/master/sflash)

 ```sh
 sflash cm_mcu.bin -c /dev/ttyUL1 
 ```

(On the Zynq, it is installed by default and is in the CMS users' path.)

## If the MCU firmware is bad

If you load bad firmware onto the MCU, the way to force a reload is to interrupt the boot loader. Do the following.

1. Open two shells on the Zynq. Open minicom in one of them and BUTool in the other.
   * Minicom: `minicom -D /dev/ttyUL1 -w -c on`
   * BUTool: `BUTool -a`
1. Force a reset of the MCU by toggling the reset line by typing the following commands in the BUTool window:

   ```sh
   write CM.CM_1.CTRL.ENABLE_UC 0
   write CM.CM_1.CTRL.ENABLE_UC 1
   ```

1. In the minicom window, you should see a message from the bootloader and a small promt that is denoted by a `$` and looks something like this:

   ```text
   ***** CM MCU BOOTLOADER *****
   v0.99.1-14-g6cce
   $ 
   ```

   This is the bootloader shell prompt. Type `f` here for "force update" or `h` for help. Help gives the following:

   ```text
   ***** CM MCU BOOTLOADER *****
   v0.99.1-14-g6cce
   $ h
   h
   This help.
   b
   Start normal boot process
   r
   Restart MCU
   f
   Force update
   ```

   After a few seconds without input, the boot loader prints out a message "CLI timed out" and hands off to the main program. The "force update" `f` option puts the MCU into a mode where it will wait for a new binary to be loaded. The minicom window will now be in a special mode where it is waiting for a new binary to be loaded, and does not respond to typed input. Exit minicom by typing `C-x q` (Control-x followed by q).

1. Then you can use the `sflash` command to load the new binary.

   ```text
   sflash cm_mcu.bin -c /dev/ttyUL1 -x
   ```

   The `-x` option tells `sflash` that the MCU is already in the bootloader mode.
   Here, `cm_mcu.bin` is the name of the binary that you want to load into the microcontroller, and the tty device is the device that you connect to to see the CLI.  

## Rev2 note

On this version, you need to leave the UART w/o doing a reset before running `sflash` (C-x q on minicom).

## New XML files

With the new firmware, the address tables that allow the SM Zynq to interpret the data coming from the MCU must also be updated.  For a release, a tar file is generated called `CornellCM_MCU.tar.gz` that contains the new XML files.  These files should be extracted and placed in the `/fw/CM` directory on the Zynq.  The files are:

```text
% tree CornellCM_MCU
CornellCM_MCU
└── address_table
   ├── address_apollo.xml
   ├── connections.xml
   └── modules_CM_MCU
      ├── PL_MEM_CM.xml -> PL_MEM_CM_Rev2.xml
      ├── PL_MEM_CM_rev1.xml
      └── PL_MEM_CM_rev2.xml
```

The `PL_MEM_CM.xml` file is a symbolic link to the correct version of the file.  The `connections.xml` file is the same for all versions of the firmware.
