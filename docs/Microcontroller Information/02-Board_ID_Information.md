# Board ID information

The board ID info can be stored on the tm4c internal EEPROM. We are using Block 1 (0x40-0x80). The ID information is stored in the following format:

| 2 bytes | 2 bytes | 4 bytes |  4 bytes | ... |
| --------------| -------| ----    |----  |---  |
| Serial #| Revision # | Firefly configuration| PS ignore mask | Other |

Read access to the board ID information in the EEPROM can occur as bytes, words or half-words, but write access can only occur as words.

Firefly configuration is organized as one 4-byte word with each of the least significant 25 bits corresponding to a firefly, such that the i-th bit from the right is 1 if the i-th firefly is populated, and 0 if it is not:

| N/A | ... | N/A | V12 RX | ... | K02 TX | K01 RX |  K01 TX |
| ----| ----| ----| ------ | ----| -------| -------| --------|
| 0   | ... | 0   | 0/1    | ... | 0/1    | 0/1    | 0/1     |

PS ignore mask is the mask of power supplies to get ignored in the MCU on the startup sequence; these will not be enabled in the power-up or disabled in the power-down.

Block 1 is password-protected so that the ID information is not accidentally rewritten. To set the password protection on a new board, use the CLI command `set_id_password`, which automatically sets the password as **0x12345678**. To write to this block, use the CLI command `set_id <password> <addr> <data>`. To view the ID information, use the CLI command `id`.

To set the firefly configuration register, type `set_id 12345678 44 <mask>`, where 12345678 is the password as mentioned in the previous paragraph, and 0x44 is the address for this configuration register.

To set the serial number, you need to do a read-modify-write since it is in a word with the rev number. TBA.

## Board ID addresses

This is a summary of the register addresses in this part of the EEPROM.

| Address | Content |
| --- | --- |
| 0x40 | ID |
| 0x44 | Firefly Mask |
| 0x48 | Power Supply Ignore Mask |
