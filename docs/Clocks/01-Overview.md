# Apollo CM Clocking and TCDS2 Overview

<img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/apollo_cm_tcds_rev3.png" alt="TCDS2 Clocking Block Diagram" width="90%" >


The above diagram shows the clock distribution and TCDS data streams between the two FPGAs. The jumper setting where FPGA1 is the endpoint is the default configuration. 

In addition to these three clock synthesizers (R1A, R1B, and R1C) there are two additional ones (R0A and R0B) that are used for reference and utility clock distribution. 

<img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/apollo_cm_clock_rev3.png" alt="Ref and util Clocking Block Diagram" width="90%" >
