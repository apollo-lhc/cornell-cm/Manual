# Programming the Clock Synthesizers

The command module has two sets of clock synthesizers that need to be programmed. R0A is a SiLabs 5341 chip, and R0B, R1A, R1B, R1C are Si 5395 chips. 

The clock programs are stored in a [Github repository](https://github.com/apollo-lhc/Cornell_CM_Rev2_HW/tree/master/ClockBuilderPRO). A summary of the available programs is in [ConfigurationIndex.xlsx](https://github.com/apollo-lhc/Cornell_CM_Rev2_HW/blob/master/ClockBuilderPRO/projects/ConfigurationIndex.xlsx) file. 

The programming of chips is done by the microcontroller on boot. The microcontroller reads the configuration from the EEPROM and programs the chips. The microcontroller also has the capability to reprogram the chips on the fly using the `loadclk` command. A single program per clock is stored in the EEPROM. The current status of the clock synthesizers can be read using the `clkmon` command. (in each case, supply an argument to the command to specify which clock to read. Valid values run from 0-4, corresponding to R0A, R0B, R1A, R1B, R1C respectively).

Sample output from the `clkmon` command is shown below, from the MCU CLI console.

```
% clkmon 0
Monitoring SI clock with id R0A
REG_TABLE       REG_ADDR BIT_MASK  VALUE 
PN_BASE         : 0x0002   0xffff    0x5341
DEVICE_REV      : 0x0005   0x00ff    0x0003
I2C_ADDR        : 0x000b   0x00ff    0x0074
STATUS          : 0x000c   0x0035    0x0000
LOS             : 0x000d   0x0015    0x0010
LOSIN_FLG       : 0x0012   0x000f    0x0000
STICKY_FLG      : 0x0011   0x002f    0x0000

Program (read from clock chip): R0Av0004
Program (read from eeprom): R0Av0004
```
On boot, if the Si chip is unprogrammed, its program read from the clock chip will be `5395ABP1` (this appears to be the default for the SiLabs part.)

## Updating the EEPROM
The external EEPROM that stores the clock infomation is an 24CS512. The EEPROM is connected to the I2C bus of the microcontroller. The EEPROM is programmed from the Zynq console using the `LoadClkConfig_tripletEEPROM.py` script. The script takes the clock program as an argument and programs the EEPROM. Example usage is shown below.
```
python LoadClkConfig_tripletEEPROM.py R1A Si5395-RevA-R1Av0003-Registers.csv --tty ttyUL1 --quiet
```
The script is installed by default on the Zynq and is also available on github in the [apollo-lhc/mcu_tools](https://github.com/apollo-lhc/mcu_tools) repository.
