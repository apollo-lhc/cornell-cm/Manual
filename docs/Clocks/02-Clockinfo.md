# Information for Si Chips
Documentation for the Si5341 and Si5395 chips. Note that the sticky versions must be cleared explicitly.

## Status registers, Si5341
On Rev2, RA0 is a Si5341 chip. Information is from table 15.11 and following on the SiLabs datasheet.

| Register name (documentation)| Address | Grafana name| 
| ---                   | ---     | --- | 
| Status Bits           | 0x0C    | STATUS_OR_LOSXAXB | 
| Loss of Signal Alarms | 0x0D    | LOS_OR_LOSOOF_IN |
| Sticky Status        | 0x11    |  STICKY_FLG | 
| Sticky Loss of Signal | 0x12    | LOSIN_FLG_OR_LOL |

### Detailed register information
#### Status Register (0x0C)

| Bit | Name | Description |
| --- | ---  | ---         |
| 0  | SYSINCAL  | 1 if device is calibrating |
| 1   | LOSXAXB  | 1 if there is no signal at the XA pin as the LOS detector is only connected to the XA pin. |
| 2   | LOSREF   | 1 if the Phase Frequency detector does not have a signal from XAXB, IN2, IN1, or IN0. | 
| 3   | LOL | 1 if the PLL is out of lock | 
| 5  | SMBUS_TIMEOUT | 1  if there is an SMBUS timeout error |

#### Loss of signal (LOS) Alarms (0x0D)
| Bit | Name | Description |
| --- | ---  | ---         |
| 3:0 | LOSIN | 1 if no clock is present at [FB_IN, IN2, IN1, IN0], respectively |

## Status registers, Si5395
All clocks except R0A are Si5395 chips. Information is from table 16.8 and following on the SiLabs datasheet.

| Register name         | Address | Grafana name|
| ---                   | ---     | --- |
| Status Bits           | 0x0C    | STATUS_OR_LOSXAXB | 
| Out of Frequency and Loss of Signal Alarms | 0x0D    | LOS_OR_LOSOOF_IN |
| Holdover and Loss of Lock Status | 0x0E | LOSIN_FLG_OR_LOL |
| Sticky Status        | 0x11    |  STICKY_FLG | 

### Detailed register information
#### Status Register (0x0C)

| Bit | Name | Description |
| --- | ---  | ---         |
| 0  | SYSINCAL  | 1 if device is calibrating |
| 1   | LOSXAXB  | 1 if there is no signal at the XAXB pins. |
| 3   | XAXB_ERR | 1 if there is a problem locking to the XAXB input signal. | 
| 5  | SMBUS_TIMEOUT | 1  if there is an SMBUS timeout error |

#### Out of Frequency and Loss of Signal Alarms (0x0D)

| Bit | Name | Description |
| --- | ---  | ---         |
| 3:0 | LOSIN | 1 if no clock is present at [IN3, IN2, IN1, IN0], respectively |
| 7:4 | OOFIN | 1 if the input frequency is out of range at [IN3, IN2, IN1, IN0], respectively |

### Holdover and Loss of Lock Status (0x0E)

| Bit | Name | Description |
| --- | ---  | ---         |
| 1  | LOL | 1 if the PLL is out of lock |
| 5  | HOLD | 1 if the device is in holdover mode |

For both devices, the sticky versions have the same bit definitions as the regular registers. They must be cleared explicitly.
