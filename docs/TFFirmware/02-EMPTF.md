# How to build and test EMP-integrated TF chains

The following section details how to build EMP-integrated track finder firmware.

## TF Firmware build environment setup 

The prerequisites are as follows:

 * Xilinx Vivado 2019.2 or 2020.1 (HLS build) and 2021.2 or newer (project build)
 * Python 3 and selected packages
 * ipbus/uHAL 2.8.10
 * ipbb: The [IPbus Builder Tool](https://github.com/ipbus/ipbb). Tested with 1.0a0+2022.dev7
 * Be a member of the `emp-fwk-users` and `cms-tcds2-users` groups and have an ssh key for your gitlab.cern account

To become a member of `emp-fwk-users` and `cms-tcds2-users`, join from the [e-groups site](https://groups.cern.ch/Pages/default.aspx), though note that enrollment may take up to 24 hours to go through.

It is recommended the other software be setup using a apptainer (singularity) container. The first time setting this up, one should run (assuming an alma9 machine)
```sh
apptainer remote login --username <username> docker://gitlab-registry.cern.ch
env apptainer shell --bind /nfs/opt/Xilinx,/nfs/cms/tracktrigger,/mnt/scratch1 docker://gitlab-registry.cern.ch/cms-cactus/phase2/firmware/docker/alma9/fw-builder:ipbb-dev-2024a-17c77887__ipbb2024a
eval `ssh-agent -s`
ssh-add
```

Different containers are available for other systems, see [here](https://gitlab.cern.ch/cms-cactus/phase2/firmware/docker/container_registry/). After the first time, one should be able to activate the container by simply running

```sh
env apptainer shell --bind /nfs/opt/Xilinx,/nfs/cms/tracktrigger,/mnt/scratch1 docker://gitlab-registry.cern.ch/cms-cactus/phase2/firmware/docker/alma9/fw-builder:ipbb-dev-2024a-17c77887__ipbb2024a
```

Instructions for setting up the environment without a container are given in the appropriate appendix.

## Building the TF firmware

To make EMP projects with the track finding firmware, you can run the following commands. Note that the git and branches for the other repositories are rather specific to ensure compatibility. These versions will work with EMP software version v0.9.0-alpha3, but will not work with v0.9.0+.

```sh
#!/usr/bin/env bash
set -e -x

# uses ssh key authentication for gitlab.cern.ch, make sure you have that set up
export PATH=/opt/cactus/bin/uhal/tools:$PATH
export LD_LIBRARY_PATH=/opt/cactus/lib:${LD_LIBRARY_PATH}

EMP_BUILD_PATH="IntegrationTests/DualFPGA"
PROJ_DIR=emp_tf

#change to match your Vivado locations
function set_vivado2019_2() {
  source /nfs/opt/Xilinx/Vitis/2019.2/settings64.sh
}

function set_vivado2022_1() {
  source /nfs/opt/Xilinx/Vitis/2022.1/settings64.sh
}

[ -d ${PROJ_DIR} ] && rm -rf ${PROJ_DIR}
ipbb init ${PROJ_DIR}
cd ${PROJ_DIR}
ipbb -e add git ssh://git@gitlab.cern.ch:7999/p2-xware/firmware/emp-fwk.git -r 0b0a54def0bab8f0740eec9253c6b0aba00dff85
ipbb -e add git https://github.com/apollo-lhc/CM_FPGA_FW -b v3.0.0

ipbb add git https://gitlab.cern.ch/ttc/legacy_ttc.git -b v2.1
ipbb add git ssh://git@gitlab.cern.ch:7999/cms-tcds/cms-tcds2-firmware.git -b v0_1_1
ipbb add git ssh://git@gitlab.cern.ch:7999/hptd/tclink.git -r fda0bcf07c501f81daeec1421ffdfb46f828f823
ipbb add git https://github.com/ipbus/ipbus-firmware -b v1.9
ipbb add git ssh://git@gitlab.cern.ch:7999/dth_p1-v2/slinkrocket_ips.git -b v03.12
ipbb add git ssh://git@gitlab.cern.ch:7999/dth_p1-v2/slinkrocket.git -b v03.12
ipbb add git https://github.com/cms-L1TK/firmware-hls.git -b dual_fpga2_4

#build HLS
set_vivado2019_2
cd src/firmware-hls
#the below command makes the HLS for both FPGAs. To speed things up when
#testing one FPGA, you can use the make rules 'fpga1' or 'fpga2'
make -C $EMP_BUILD_PATH/firmware
cd -
unset LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/opt/cactus/lib:${LD_LIBRARY_PATH}
set_vivado2022_1

# FPGA 1 project for simulation
ipbb proj create vivado vsim_f1 firmware-hls:IntegrationTests/DualFPGA/ vsim_f1.dep
cd proj/vsim_f1/
ipbb vivado generate-project --single
cd -

# FPGA 2 project for simulation
ipbb proj create vivado vsim_f2 firmware-hls:IntegrationTests/DualFPGA/ vsim_f2.dep
cd proj/vsim_f2/
ipbb vivado generate-project --single
cd -

# FPGA 1 project for implementation
ipbb proj create vivado tf_f1 firmware-hls:IntegrationTests/DualFPGA/ apollo_f1.dep
cd proj/tf_f1/
ipbb ipbus gendecoders
ipbb vivado generate-project --single
ipbb vivado synth -j16 impl -j16 package
cd -

# FPGA 2 project for implementation
ipbb proj create vivado tf_f2 firmware-hls:IntegrationTests/DualFPGA/ apollo_f2.dep
cd proj/tf_f2/
ipbb ipbus gendecoders
ipbb vivado generate-project --single
ipbb vivado synth -j16 impl -j16 package
cd -
```

To run simulation once the project is created, open the project `proj/vsim_f*/vsim_f*/vsim_f*.xpr` with Vivado, launch the simulation, and run for at least 60 microseconds. The simulation output will be saved at `proj/vsim_f*/vsim_f*/vsim_f*.sim/sim_1/behav/xsim/out.txt`. You can ex. compare the output from the simulated firmware with the reference from the CMSSW emulation produced above by using 

```sh
./script/check_fpga*_output.py -e 0-99 -r mem/emData/MemPrints -c out.txt
```

Although the second FPGA simulation defaultly uses an input generated from the emulation, you can instead use the FPGA1 simulation output as an input by running

```sh
./script/generate_fpga2_input.py -i out.txt -o mem/in_fpga2.txt
```

## Testing on an apollo blade

Copy the two tgz files (generated in the package directory of the project folder after running `ipbb vivado package`) to the apollo blade, along with desired stub input files, which can be found at `src/firmware-hls/IntegrationTests/DualFPGA/firmware/mem/in_fpga1_*.txt`. 

Setup your environment using the appropriate `emp-toolbox` script:
```sh
source /opt/emp-toolbox/scripts/env.sh
```
If `emp-toolbox` is not already installed consult [this page](https://apollo-lhc.gitlab.io/SM_Software/emp-toolbox/) for installation instructions.

You can then load the firmware to the CM FPGAs with the following commands. Assuming you have soft-linked the tgz files to tf_f1.tgz and tf_f2.tgz, you would run:

```sh
uio_send.py program-fpga -f ./tf_f1.tgz
uio_send.py program-fpga -f ./tf_f2.tgz
```

Interfacing with the FPGAs is performed with a software tool called `empbutler`. For `empbutler` to work and to perform inter-FPGA tests, which requires a TCDS-distributed clock, run the following commands in `BUTool`:

```text
BUTool.exe -a <<EOF
unblockAXI
write SERV.CLOCKING.HQ_SEL 0
write SERV.TCDS.TTC_SOURCE 0
exit
EOF
```

The following set of commands shows how to setup the interFPGA links, inject data into the FPGA1 RX buffers, and read data from the FPGA2 TX buffers. This requires a connection file, which may already exist, but if not, see the appendix below on how to set up the connection and address files. More information on `empbutler` and allowed commands can be found [here](https://serenity.web.cern.ch/emp-fwk/software/index.html). The link numbers here are given by `n=4*i+j` where `i` is the EMP region code and `j` runs from 0-3. See [inter-fpga link numbering](../MGTs/03-Inter_FPGAs.md) for more details.

```sh

# all links going from F1->F2
export RX_LINKS_F1="68-79,84-119"
export TX_LINKS_F1="04-15,20-54"
export RX_LINKS_F2="09-43,48-59"
export TX_LINKS_F2="92-93"
  
#reset firmware, configure and align links
empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS reset tcds2
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS reset tcds2
empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS mgts configure tx -c $TX_LINKS_F1
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS mgts configure rx -c $RX_LINKS_F2
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS mgts align -c $RX_LINKS_F2
empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS mgts status tx -c $TX_LINKS_F1
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS mgts status rx -c $RX_LINKS_F2

#play data from F1 RX buffer and capture output from F2 TX buffer
empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS buffers rx PlayOnce -c $RX_LINKS_F1 --inject file:in_fpga1_0.txt
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS buffers tx Capture -c $TX_LINKS_F2 --bx-range 137
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS capture --tx $TX_LINKS_F2
```

The `empbutler` `capture` command saves the data recorded in the buffers to `data/tx_summary.txt`. This text file can be then be analyzed. 

For example, one can compare the output in simulation and in hardware using the script 

```sh
/src/firmware-hls/IntegrationTests/common/scripts/emp/check_fpga2_output.py -e 0-8 -r out.txt -c tx_summary.txt 
```

where `tx_summary.txt` has been copied from the apollo blade and `out.txt` is the simulation output from Vivado (typically generated in ex. `/proj/vsim_f2/vsim_f2/vsim_f2.sim/sim_1/behav/xsim/out.txt`).

## Appendix: obtaining emulation output for comparison

To obtain the emulated output for comparison, clone [this repository](https://github.com/mcoshiro/emp_build_script) and run the script `make_cmssw_memprints.sh`, which will generate a file `pre.txt` that contains the expected output from the track trigger project in the directory `CMSSW_13_3_0_pre2/src/L1Trigger/TrackFindingTracklet/test`. By default, it will generate output for the reduced chain, but you can get output for the barrel chain by running `make_cmssw_memprints.sh barrel`. Note that these files contain the output for all sectors rather than just one sector, which is what would be implemented on a single board.

## Appendix: Firmware build hacks and pitfalls

As the second FPGA project is currently just on the edge of meeting timing, the first (default) run above may not achieve timing closure. In that case, you may have to rerun from Vivado with an alternate implementation strategy, or using non-project mode. After you generate a bitfile in this way, you can generate the tar file by copying your bitfile to `proj/tf_f2/products/tf_f2.bit`, updating it's last modified time so ipbb doesn't attempt to rebuild it, and using the `ipbb vivado package` command. This results in commands that typically look like

```sh
cd proj/tf_f2/
rm products/tf_f2.bit
rm products/tf_f2.svf
cp tf_f2/tf_f2.runs/impl_1/top.bit products/tf_f2.bit
touch products/tf_f2.bit
ipbb vivado package
```

Furthermore, EMP doesn't always pull the overlays for the correct FPGA. We will look into fixing this, but currently, one can circumvent this by fixing the tar source and retarring:

```sh
cd proj/tf_f2/package
rm src/addrtab/apollo_package.tgz
cp <other directory>/apollo_package.tgz src/addrtab/
mv src tf_f2
tar -czvf tf_f2.tgz tf_f2
mv tf_f2 src
```

## Appendix: possible issues on Apollo

If `uio_send.py` fails to load firmware on the FPGAs, you can try the following debugging steps.

 - restart the UIO daemon with `systemctl restart uio_daemon`
 - power cycle the FPGAs using `BUTool`. The `pwrup` may not always work, so one should check the power status using the `status` command:

```text
BUTool.exe -a <<EOF
cmpwrdn 2
cmpwrdn 1
cmpwrup 1
cmpwrup 2
status
exit
EOF
```

 - in some rare cases, the Zynq may need to be rebooted to power on the CM FPGAs

## Appendix: setting up connections and address files

To set up the address and connnections files, extract the firmware `tgz` files and copy the `addrtab` directories. For this example, I'll assume we have copied them to `/home/cms/user/addrtab_f1` and `/home/cms/user/addrtab_f2`. One can then make a `connections.xml` file with the following content:

```xml
<connections>
  <connection id="F1_IPBUS" uri="ipbusmmap-2.0:///dev/uio_F1_IPBUS?offset=0x0" address_table="file:///home/cms/user/addrtab_f1/top_emp.xml" />
  <connection id="F2_IPBUS" uri="ipbusmmap-2.0:///dev/uio_F2_IPBUS?offset=0x0" address_table="file:///home/cms/user/addrtab_f2/top_emp.xml" />
</connections>
```

With this, one can point `empbutler` to the `connections.xml` file to correctly inform it of the EMP IPBUS address structure.

## Appendix: Setting up firmware build environment via local installs

The instructions in this section detail how local installation can be performed, which was used prior to the apptainer environment.

To install uHAL 2.8.10, you can use the following commands, which assume installation in `/mnt/scratch1/<username>/linux/opt/`.

```sh
cd /mnt/scratch1/<username>
git clone --depth=1 -b v2.8.10 --recurse-submodules https://github.com/ipbus/ipbus-software.git
cd ipbus-software/extern/pugixml
wget https://github.com/zeux/pugixml/releases/download/v1.13/pugixml-1.13.zip ./

#change PACKAGE_VER_MAJOR/MINOR to 1/13 and change the unzip command to just "unzip -q -u ${ZIP_FILE} \"
sed -i 's/PACKAGE_VER_MINOR = 2/PACKAGE_VER_MINOR = 13/g' Makefile
sed -i 's/unzip -q -u ${ZIP_FILE} -d ${ZIP_NAME};/unzip -q -u ${ZIP_FILE};               /g' Makefile

make BUILD_PUGIXML=1 Set=uhal
cd ../..
make install prefix=/mnt/scratch1/<username>/linux/opt/cactus Set=uhal
cp extern/pugixml/pugixml-1.13/libpugixml.so /mnt/scratch1/<username>/linux/opt/cactus/lib/

#change default template to /mnt/scratch1/<username>/linux/opt/cactus/etc/uhal/tools/ipbus_addr_decode.vhd
cd /mnt/scratch1/<username>/linux/opt/cactus/bin/uhal/tools
sed -i 's/\/opt\/cactus\//\/mnt\/scratch1\/<username>\/linux\/opt\/cactus\//g' gen_ipbus_addr_decode

#modify bash_profile to add /mnt/scratch1/<username>/linux/opt/cactus/bin/uhal/tools to PATH
# and /mnt/scratch1/<username>/linux/opt/cactus/lib to LD_LIBRARY_PATH
vim ~/.bash_profile 

#add /mnt/scratch1/<username>/linux/opt/cactus/lib/python2.7/site-packages to PYTHONPATH
vim ~/.bashrc 
```

To install ipbb in `/mnt/scratch1/<username>/linux/usr/lib/python<version>/site-packages`, run the following command:

```sh
python3 -m pip install --target=/mnt/scratch1/<username>/linux/usr/lib/python<version>/site-packages https://github.com/ipbus/ipbb/archive/dev/2022g.tar.gz
```

Note that the click dependecy of ipbb (and ipbus-tools, see below) is particularly prone to breaking compatibility. If python installs a version of click greater than 8.0, you can force a reinstall with

```sh
python3 -m pip install --force-reinstall --upgrade --target=/mnt/scratch1/<username>/linux/usr/lib/python<version>/site-packages click==7.1.2
```

While pip should take care of the ipbb dependencies, other uHAL dependencies such as `pyyaml`, `yamllint`, `lxml`, and `Jinja2` may not be installed. If you run into any missing dependencies, simply install them with pip as above.
