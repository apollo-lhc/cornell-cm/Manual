# EMP Build for a CSP test firmware
1. Set up all directories/repositories that will be used to compile the firmware that we will use to program the FPGA:
Set up the container.  When setting up the container you must be sure to include the directories containing your python and vivado installations, /nfs/cms/tracktrigger , and what ever other directories you plan to use while generating the firmware.  Here is an example:
```
env apptainer shell --bind /nfs/opt/Xilinx,/cdat/tem/pw94/miniconda,/nfs/cms/tracktrigger,/mnt/scratch1/ad683 docker://gitlab-registry.cern.ch/cms-cactus/phase2/firmware/docker/fw-builder:2023-08-08__ipbb2023a
scl enable rh-git218 bash 
```
The second command generates a subshell that allows the git permissions to function.
A rough guide for the rest of this step can be found at the top of this [google document](https://docs.google.com/document/d/1gP5a4Yq41jYzAz1HQdFpO94b1P6fcPojqGm1Iu10sBo/edit).

It is accurate except for the following errors: clone branch v2.2.1  of CM_FPGA_FW instead of develop
Before starting the process of generating the firmware (also found in the above guide, beginning with subtitle # FPGA1) replace the file `emp-fwk/components/payload/firmware/hdl/emp_payload.vhd`  from the emp-fwk repository you just cloned with the file found at the bottom [of this link](https://serenity.web.cern.ch/serenity/emp-fwk/firmware/backend-links.html#payload-port-specification).

2. Begin compiling the firmware (found in the above google doc, follow sections titled # FPGA1 and # FPGA2).
After generating the firmware, copy the resulting bit or svf file to a place that allows you to program the FPGAs

3. Generate the tarball to be used by EMPbutler
Using the CM_FPGA_FW repository with branch v2.2.1 that you cloned earlier (or you can clone a new one elsewhere), fix the following issues and generate the tarball:
```
#fix which address tables are referenced
cd CM_FPGA_FW/kernel/address_tables
rm address_table
ln -s address_table_EMP_Cornell_rev2_p1_VU13p-1-SM_USP address_table
cd -

#fix makefile and generate tarball
cd CM_FPGA_FW/
echo 'EMP%.tar.gz : kernel/config_EMP%.yaml' >> Makefile
echo '  $(MAKE) overlays' >> Makefile
echo '  @tar -h -zcf $@ -C kernel/hw/ dtbo -C ../address_tables address_table' >> Makefile
make prebuild_EMP_Cornell_rev2_p1_VU13p-1-SM_USP
make EMP_Cornell_rev2_p1_VU13p-1-SM_USP.tar.gz
cd -
```
Do this for both FPGAs (p1 and p2), so you have a tarball for both.
Now, move the tarballs to a location where they can be accessed by EMPbutler. I moved them to `/opt/emp-toolbox-v0_6_17/uio_tests/EMP_Cornell_rev2_p1_VU13p-1-SM_USP.tar.gz` on the apollo205

4. Generate a connections xml file on the board you wish to test CSP and the following lines to it (I put mine in /fw/CM/connection.xml on apollo205):
```
<connections>
 <connection id="apollo.c2c1.vu13p" uri="ipbusmmap-2.0:///dev/uio_F1_IPBUS?offset=0x0" address_table="file:///fw/CM/vu13p_p1_CSP/vu13p_p1_lnx4108_classe_cornell_edu_231026_1542/addrtab/top_emp_fpga1.xml" />
 <connection id="apollo.c2c2.vu13p" uri="ipbusmmap-2.0:///dev/uio_F2_IPBUS?offset=0x0" address_table="file:///fw/CM/vu13p_p2_CSP/vu13p_p2_lnx4108_classe_cornell_edu_231027_1633/addrtab/top_emp_fpga2.xml" />
</connections>
```
5. Clone a repository of emp-toolbox on the board you wish to test CSP with the following commands.
```
git clone --recurse-submodules https://:@gitlab.cern.ch:8443/p2-xware/software/emp-toolbox.git -b v0.7.5
cd emp-toolbox

sudo yum remove "cactusboards-emp-*"
sudo cp docs/yum-dependencies/*.repo /etc/yum.repos.d/
sudo yum install boost-devel pugixml-devel python-devel cactuscore-uhal-* cactuscore-build-utils-0.2.9 tkinter
sudo python3.6 -m pip install --upgrade click==8.0.4 click_didyoumean==0.3.0 pytest==7.0.1 python-dateutil==2.8.2 pyyaml==5.1.2 colorama==0.4.5

make
source scripts/env.sh
```
This branch of emp-toolbox must be used to ensure compatibility with the firmware.
I was having trouble getting it to clone properly, so I cloned it to the lnx4189 only to scp this directory to the apollo205 later and that worked

6. Finally, program the boards with the bit/svf file you created in step 2, then run the following commands:
```
cd /opt/emp-toolbox-v0_7_5
export UHAL_ENABLE_IPBUS_MMAP=1
uio_send.py load-tar -f /opt/emp-toolbox-v0_6_17/uio_tests/EMP_Cornell_rev2_p1_VU13p-1-SM_USP.tar.gz
uio_send.py load-tar -f /opt/emp-toolbox-v0_6_17/uio_tests/EMP_Cornell_rev2_p2_VU13p-1-SM_USP.tar.gz
cd /opt/emp-toolbox-v0_7_5 && source ./scripts/env.sh
```

```
which empbutler        # (this line is only a test to make sure EMPbutler is working, not essential)
```
```
export PATH=/opt/cactus/bin/emp:$PATH
export LD_LIBRARY_PATH=/opt/cactus/lib:$LD_LIBRARY_PATH

empbutler -c /fw/CM/connection.xml do apollo.c2c1.vu13p reset internal      #   (if SIGBUS error, reboot the board and unblockAXI and try again)
empbutler -c /fw/CM/connection.xml do apollo.c2c2.vu13p reset internal

empbutler -c /fw/CM/connection.xml do apollo.c2c1.vu13p buffers tx PlayOnce -c 4,5 --inject generate:counter-with-index
empbutler -c /fw/CM/connection.xml do apollo.c2c1.vu13p mgts configure tx -c 4,5

empbutler -c /fw/CM/connection.xml do apollo.c2c2.vu13p mgts configure rx -c 58,59
empbutler -c /fw/CM/connection.xml do apollo.c2c2.vu13p mgts status rx -c 58,59
empbutler -c /fw/CM/connection.xml  do apollo.c2c2.vu13p mgts align -c 58,59
empbutler -c /fw/CM/connection.xml do apollo.c2c2.vu13p buffers rx Capture -c 58,59
empbutler -c /fw/CM/connection.xml do apollo.c2c2.vu13p capture --rx 58,59
```
This should yield an `injected_data.txt` file containg the data you injected into the transmitter and a `data/rx_summary.txt` file that contains what the reciever read.
This final step does not work as of yet.  Using internal  for the reset commands yields a blank output file, and we do not have a DTH to use tcds2 on  apollo205.
