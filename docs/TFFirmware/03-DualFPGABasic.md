# Basic Dual-FPGA firmware for TF algorithm setup

This document describes the basic setup for the dual-FPGA firmware for the TrackFinder algorithm. This setup is used for the basic tests of the TrackFinder algorithm.

## Build node at Cornell

At Cornell it is suggested to use lnx619.classe.cornell.edu. Scratch disk is under `/mnt/scratch1/$USER`. You can make a directory for your work under `/mnt/scratch1/$USER` and work there.  This disk is *not* backed up.

## Set up the environment

It is suggested to use a container to set up the environment. The following commands can be used to set up the container (the image is new as of 7/2024). If you have not previously setup apptainer, log in to your gitlab.cern.ch account:

```sh
apptainer remote login --username <username> docker://gitlab-registry.cern.ch
```

Then open a shell in the container:

```sh
env apptainer shell --bind /nfs/opt/Xilinx,/nfs/cms/tracktrigger,/mnt/scratch1 docker://gitlab-registry.cern.ch/cms-cactus/phase2/firmware/docker/alma9/fw-builder:ipbb-dev-2024a-17c77887__ipbb2024a
```

This is an alma9 container, since lnx619 is an alma9 machine. It is one of the official EMP containers. 

Then in the container, you can set up the environment with the following commands:

```sh
source /nfs/opt/Xilinx/Vivado/2020.2/settings64.sh
# need to add ssh key for gitlab.cern.ch access 
eval `ssh-agent -s`
ssh-add
```

The `ssh-add` is needed because we use ssh key authentication for gitlab.cern.ch. To test if your ssh key is working properly, you can run `ssh -T git@gitlab.cern.ch -p 7999`. You should see a message like `Welcome to GitLab, @username!`.

## Setup and build script

See below as an example. This uses a commit at the head of `emp-fwk` and the v3.0.0 tag for `CM_FPGA_FW`. The `apollo_f1.dep` and `apollo_f2.dep` files are used to specify the dependencies for the firmware. These dep files are in the `firmware-hls` repository, under the directory `IntegrationTests/DualFPGA/`.

This script first builds for F1 and then builds for F2. Note that this does not build the HLS code; a manual `make` command needs to be integrated and the resulting cores and other glue code are currently not in this particular repo directory.

```sh
#/bin/bash
set -e -x

# uses ssh key authentication for gitlab.cern.ch, make sure you have that set up
export PATH=/opt/cactus/bin/uhal/tools:$PATH 
export LD_LIBRARY_PATH=/opt/cactus/lib:${LD_LIBRARY_PATH}

PROJ_DIR_BASE=dual_fpga_v0

FPGAS="f1 f2"

for FPGA in $FPGAS; do
    PROJ_DIR=${PROJ_DIR_BASE}_${FPGA}
    [ -d ${PROJ_DIR} ] && rm -rf ${PROJ_DIR}
    ipbb init ${PROJ_DIR}
    cd ${PROJ_DIR}
    # from Jonni 7/2024
   # from Jonni 7/2024
   ipbb -e add git ssh://git@gitlab.cern.ch:7999/p2-xware/firmware/emp-fwk.git -r 0b0a54def0bab8f0740eec9253c6b0aba00dff85
   ipbb -e add git https://github.com/apollo-lhc/CM_FPGA_FW -b v3.0.0


   ipbb add git https://gitlab.cern.ch/ttc/legacy_ttc.git -b v2.1
   ipbb add git ssh://git@gitlab.cern.ch:7999/cms-tcds/cms-tcds2-firmware.git -b v0_1_1
   ipbb add git ssh://git@gitlab.cern.ch:7999/hptd/tclink.git -r fda0bcf07c501f81daeec1421ffdfb46f828f823
   ipbb add git https://github.com/ipbus/ipbus-firmware -b v1.9
   ipbb add git ssh://git@gitlab.cern.ch:7999/dth_p1-v2/slinkrocket_ips.git -b v03.12
   ipbb add git ssh://git@gitlab.cern.ch:7999/dth_p1-v2/slinkrocket.git -b v03.12
   ipbb add git ssh://git@github.com/cms-l1tk/firmware-hls.git -b dual_fpga


   BASEDIR=`pwd`

   export COLUMNS=120
   # F1
   ipbb proj create vivado tf_${FPGA} firmware-hls:IntegrationTests/DualFPGA/ apollo_${FPGA}.dep
   cd proj/tf_${FPGA}
   ipbb ipbus gendecoders
   ipbb vivado generate-project --single
   ipbb vivado synth -j16 impl -j16 package
done

```

At the end you should have two sets of tgz files for F1 and F2 to install.

## Installing and testing on apollo214

Copy the two tgz files to apollo214. Then you can install the firmware with the following commands. This is assuming you have soft-linked the tgz files to tf_f1.tgz and tf_f2.tgz, and the connections.xml file pointing to the untarred directories:

```text
[cms@cmsapollo214 wittich]$ ls -l tf_f[12].tgz
lrwxrwxrwx 1 cms cms 47 Jul 29 04:08 tf_f1.tgz -> tf_f1_lnx619_classe_cornell_edu_240726_1003.tgz
lrwxrwxrwx 1 cms cms 47 Jul 29 04:09 tf_f2.tgz -> tf_f2_lnx619_classe_cornell_edu_240726_1207.tgz
```

The link numbers here are given by `n=4*i+j` where `i` is the EMP region code and `j` runs from 0-3. See [inter-fpga link numbering](../MGTs/03-Inter_FPGAs.md) for more details.

```shell
#! /bin/sh
set -x
set -e

uio_send.py program-fpga -f ./tf_f1.tgz
uio_send.py program-fpga -f ./tf_f2.tgz

BUTool.exe -a <<EOF
unblockAXI
write SERV.CLOCKING.HQ_SEL 0
write SERV.TCDS.TTC_SOURCE 0
exit
EOF


# all links going from F1->F2
export TXLINKS="4-15,20-59"
export RXLINKS="4-43,48-59"
  
empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS reset tcds2
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS reset tcds2
# uncomment following line to bypass the payload
#empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS  buffers tx PlayOnce -c $TXLINKS  --inject generate:counter-with-index
empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS mgts configure tx -c $TXLINKS
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS mgts configure rx -c $RXLINKS
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS mgts align -c $RXLINKS

empbutler -c /opt/address_table/emp_connections.xml do F1_IPBUS mgts status tx -c $TXLINKS
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS mgts status rx -c $RXLINKS


empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS buffers rx Capture -c $RXLINKS
empbutler -c /opt/address_table/emp_connections.xml do F2_IPBUS capture --rx $RXLINKS
```
