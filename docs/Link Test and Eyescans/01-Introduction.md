#Introduction

<p style='text-align: right;'>
<em>Rui Zou, Nov 22 2023</em>
</p>

We use tranceivers to communicate between FPGAs and to and from other boards.
To study the quality of the link, we perform eyescans and IBERT test. 
If the eyescan opening is larger than the given mask and 0 errors are obsereved when reaching BER smaller than 1e-16 with PRBS-31 patterns, the link is considered good. If the eye opening is smaller than the mask or errors are observed before reaching BER smaller than 1e-16, a tuning of parameters should be performed. The parameters with the best performance should be recorded. Eyescan and IBERT test should be repeated on said link with best tuned parameters.

For details on how to perform eyescan and IBERT test. See [IBERT instructions](../Production%20Test/04-IBERT.md).

For details on the parameters of the links and auto-tuning. See [autotuning instructions](04-Autotuning.md).

To study the quality of a particular optical engine with Clock Data Recovery (CDR) circuit, we perform an additional margin test. Since the eyescan only reflects the path between the receiver of the optical engine and the GTY on the FPGA, we use the margin test to study the stability on the transmitter side, in addition to eyescan and IBERT test.

For details on how to do margin test. See [margin test instructions](03-MarginTest.md).

Before any of the test, you should first make sure the fireflies are enabled.
To change the settings on the fireflies, ssh into target apollo IP. Set alarm threshold to be higher on MCU. And enable the lasers on the fireflies.

        ssh cms@apolloIP
        minicom -D /dev/ttyUL1
        alm settemp ff 55
        alm settemp fpga 85  
        ff xmit on all

    To quit MC U CLI, do Ctrl+A X.

