#Eyescans Performance

<p style='text-align: right;'>
<em>Rui Zou, April 6 2023</em>
</p>

We perform eyescan test to check the quality and stability of the link. 

The eye opening should meet a certain criteria to be considered good. It's not the Area, but the horizontal and vertical margin that matters. A mask for Ultrascale GTY was provided by Xilinx employee on [the Xilinx forum](https://support.xilinx.com/s/question/0D52E00006iHnb2SAC/any-parameters-we-can-tune-gty-transceiver-to-make-eye-open-area-bigger?language=en_US).

Eyescan mask for Ultrascale GTY:

<img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/UltrascaleGTYMask.png" alt= “” width="50%" height="50%">

See below for the eyescan behavior change with regards to different Tx settings with two Rx equalizer modes, LPM & DFE.

## Lower Power Mode (LPM)
According to Xlinx GTY manual, LPM mode is recommended for short-reach and low-reflection channel applications with channel losses of 15 dB or less at the Nyquist frequency. LPM is preferred for applications where non-random data patterns are used. 

Eyescan changes with increasing Tx Pre-Cursor: 

![](https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/72a5b79c44277167c027c8aa3a519a4aeba8848f/images/eyescan_TxPre.gif "Eyescan with increasing Tx Pre-Cursor")

Eyescan changes with increasing Tx Post-Cursor:

![](https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/eyescan_TxPost.gif "Eyescan with increasing Tx Post-Cursor")

Eyescan changes with increasing Tx Diff Swing Voltage:

![](https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/eyescan_TxDiffSwing.gif "Eyescan with increasing Tx Diff Swing Voltage")

## Decision Feedback Equalizaiton (DFE)
According to Xlinx GTY manual, DFE mode is recommended for high-reflection channels, medium- to long-reach applications with high channel losses of 8 dB and above at the Nyquist frequency. DFE mode has the advantage of equalizing a channel without amplifying noise and crosstalk. Therefore, it is the best choice when crosstalk is a concern or when reflections are identified in a single-bit response analysis. 

Eyescan changes with increasing Tx Pre-Cursor: 

![](https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/eye_DFE_TxPre.gif "Eyescan with increasing Tx Pre-Cursor")

Eyescan changes with increasing Tx Post-Cursor:

![](https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/eye_DFE_TxPost_v1.gif "Eyescan with increasing Tx Post-Cursor")

Eyescan changes with increasing Tx Diff Swing Voltage:

![](https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/eye_DFE_TxDiffSwing_v1.gif "Eyescan with increasing Tx Diff Swing Voltage")


