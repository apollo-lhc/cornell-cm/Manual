#Margin Test

<p style='text-align: right;'>
<em>Rui Zou, Nov 22 2023</em>
</p>

We use IBERT to test optical integrity. There are two components: a 4-day long PRBS test (target BER: 10^-16) and eyescan. In the lab we use PRBS-31 pattern with LPM mode and an insertion loss at Nyquist frequency set at 3dB.

We run a margin test on optical engine parameters to test the stability of the Firefly part. See for instructions on how to run nominal IBERT and link test.

## Procedure

1. For margin test, go to [repository](https://github.com/apollo-lhc/Cornell_CM_Production_Scripts/tree/master). Follow [the section in the README on margin test](https://github.com/apollo-lhc/Cornell_CM_Production_Scripts/blob/master/README.md#running-the-margin-scanning-script).

    You will need to adjust the config (default: `config1D.ini`) to your local setup. For TIF setup, target0_name and target1_name should be cmsapollo214.
<details>
  <summary>Click for an example output.</summary>
  ```

    #-----------------------------------------------------------
    # Vivado v2020.1 (64-bit)
    # SW Build 2902540 on Wed May 27 19:54:35 MDT 2020
    # IP Build 2902112 on Wed May 27 22:43:36 MDT 2020
    # Start of session at: Thu Apr  7 10:20:17 2022
    # Process ID: 15385
    # Current directory: /mnt/scratch/rz393/Cornell_CM_Production_Scripts/autotuning
    # Command line: vivado -mode tcl
    # Log file: /mnt/scratch/rz393/Cornell_CM_Production_Scripts/autotuning/vivado.log
    # Journal file: /mnt/scratch/rz393/Cornell_CM_Production_Scripts/autotuning/vivado.jou
    #-----------------------------------------------------------
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    open_hw_manager
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    connect_hw_server -allow_non_jtag -url localhost:3121
    INFO: [Labtools 27-2285] Connecting to hw_server url TCP:localhost:3121
    INFO: [Labtools 27-3415] Connecting to cs_server url TCP:localhost:3042
    INFO: [Labtools 27-3414] Connected to existing cs_server.
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    disconnect_hw_server localhost:3121
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    connect_hw_server -allow_non_jtag -url localhost:3121
    INFO: [Labtools 27-2285] Connecting to hw_server url TCP:localhost:3121
    INFO: [Labtools 27-3415] Connecting to cs_server url TCP:localhost:3042
    INFO: [Labtools 27-3414] Connected to existing cs_server.
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    open_hw_target -verbose -xvc_url apollo10:2542
    INFO: [Labtools 27-2285] Connecting to hw_server url TCP:localhost:3121
    INFO: [Labtools 27-3415] Connecting to cs_server url TCP:localhost:3042
    INFO: [Labtools 27-3414] Connected to existing cs_server.
    INFO: [Labtoolstcl 44-466] Opening hw_target localhost:3121/xilinx_tcf/Xilinx/apollo10:2542
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    current_hw_device [lindex [get_hw_devices] 0]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    refresh_hw_device -update_hw_probes false [lindex [get_hw_devices] 0]
    INFO: [Labtools 27-2302] Device xcvu13p (JTAG device index = 0) is programmed with a design that has 2 IBERT core(s).
    INFO: [Labtools 27-2302] Device xcvu13p (JTAG device index = 0) is programmed with a design that has 1 JTAG AXI core(s).
    refresh_hw_device: Time (s): cpu = 00:00:09 ; elapsed = 00:01:37 . Memory (MB): peak = 3320.246 ; gain = 1051.004 ; free physical = 175 ; free virtual = 13311
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    source ./tcl/CM_VU13P_rcv_setup.tcl
    # refresh_hw_device -update_hw_probes false [lindex [get_hw_devices xcvu13p_0] 0]
    INFO: [Labtools 27-2302] Device xcvu13p (JTAG device index = 0) is programmed with a design that has 2 IBERT core(s).
    INFO: [Labtools 27-2302] Device xcvu13p (JTAG device index = 0) is programmed with a design that has 1 JTAG AXI core(s).
    refresh_hw_device: Time (s): cpu = 00:00:01 ; elapsed = 00:00:48 . Memory (MB): peak = 3322.246 ; gain = 2.000 ; free physical = 183 ; free virtual = 11605
    # set mgt_tx_list [eval get_hw_sio_txs]
    # set mgt_rx_list [eval get_hw_sio_rxs]
    # set mgt_len [llength $mgt_tx_list]
    WARNING: [Labtoolstcl 44-158] No matching hw_sio_links were found.
    # remove_hw_sio_link [get_hw_sio_links]
    # for {set i 0} {$i<$mgt_len} {incr i} {
    #     set xil_newLink [create_hw_sio_link -description "MGT $i" [lindex $mgt_tx_list $i] [lindex $mgt_rx_list $i] ]
    #     lappend xil_newLinks $xil_newLink
    # }
    # set xil_newLinkGroup [create_hw_sio_linkgroup -description {Link_Group_0} [get_hw_sio_links $xil_newLinks]]
    # unset xil_newLinks
    # eval get_hw_sio_links
    # set_property TXDIFFSWING {530 mV (00101)} [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # set_property TXPOST {0.00 dB (00000)}  [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    commit_hw_sio: Time (s): cpu = 00:00:00.10 ; elapsed = 00:00:05 . Memory (MB): peak = 3456.246 ; gain = 0.000 ; free physical = 192 ; free virtual = 11484
    # set_property TXPRE {0.01 dB (00000)} [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    commit_hw_sio: Time (s): cpu = 00:00:00.09 ; elapsed = 00:00:05 . Memory (MB): peak = 3456.246 ; gain = 0.000 ; free physical = 192 ; free virtual = 11484
    # set_property RXDFEENABLED 0 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    commit_hw_sio: Time (s): cpu = 00:00:00.11 ; elapsed = 00:00:10 . Memory (MB): peak = 3456.246 ; gain = 0.000 ; free physical = 192 ; free virtual = 11484
    # set_property PORT.GTRXRESET 1 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    commit_hw_sio: Time (s): cpu = 00:00:00.10 ; elapsed = 00:00:06 . Memory (MB): peak = 3456.246 ; gain = 0.000 ; free physical = 189 ; free virtual = 11481
    # set_property PORT.GTRXRESET 0 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # set_property PORT.GTRXRESET 1 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # set_property PORT.GTRXRESET 0 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # set_property TX_PATTERN {PRBS 31-bit} [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # set_property RX_PATTERN {PRBS 31-bit} [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    # commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    commit_hw_sio [get_hw_sio_links *MGT_X0Y31/RX]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    set_property LOGIC.RX_RESET_DATAPATH 1 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    set_property LOGIC.RX_RESET_DATAPATH 0 [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    commit_hw_sio [get_hw_sio_links -of_objects [get_hw_sio_linkgroups {Link_Group_0}]]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    set_property LOGIC.MGT_ERRCNT_RESET_CTRL {1} [get_hw_sio_links *MGT_X0Y31/RX]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    commit_hw_sio [get_hw_sio_links *MGT_X0Y31/RX]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    set_property LOGIC.MGT_ERRCNT_RESET_CTRL {0} [get_hw_sio_links *MGT_X0Y31/RX]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    commit_hw_sio [get_hw_sio_links *MGT_X0Y31/RX]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    puts [get_property LOGIC.LINK [get_hw_sio_links *MGT_X0Y31/RX]]
    1
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    puts [get_property LOGIC.ERRBIT_COUNT [get_hw_sio_links *MGT_X0Y31/RX]]
    000000000000
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    puts [get_property RX_RECEIVED_BIT_COUNT [get_hw_sio_links *MGT_X0Y31/RX]]
    18581493680
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    set xil_scan [create_hw_sio_scan -description {xil_scan} 1d_bathtub  [lindex [get_hw_sio_links *MGT_X0Y31/RX] 0 ]]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    set_property HORIZONTAL_INCREMENT 1 [get_hw_sio_scans]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    set_property DWELL_BER "1e-6" [get_hw_sio_scans]
    puts PROC_BOUNDARY
    PROC_BOUNDARY
    foreach s [get_hw_sio_scans] {
        run_hw_sio_scan $s
        wait_on_hw_sio_scan $s}
```
</details>

    To change the Tx equalization or any other settings on the firefly side, ssh into target apollo IP and change the settings via i2c. Also, dont forget to turn the lasers on the FF by doing `ff xmit on all`. See below for an example, here we change the Tx eq on link at 0x3e for the FF 4 to the value of 1, and read from the same register to confirm the change registered.

        ssh cms@apolloIP
        minicom -D /dev/ttyUL1
        % ff regw 3e 1 4
            ff: write val 0x1 to register 0x3e, FF 4.
        % ff regr 3e 4
            ff: reading FF F1_3  12 Tx, register 0x3e
            ff: Command returned 0x1 (ret 0).

    To quit MCU CLI, do Ctrl+A X.

    For 12ch 25 Gb/s parts the Tx eq registers are 0x3e -- 0x43 ((62-67) Byte) (each link has a register). You will need to change all of them for each Tx part. The values we normally try are: 0, 1 (0001), 2 (0010), 4 (0100), 7 (0111).

2. To plot the margin test result, go to the same README and continue to follow the instructions. 
    See below for an example plot for 4 ch FFs and 12 ch prototype parts.

<p float="left">
<img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/FF_4ch_Margin.png" alt= “” width="40%" height="40%"> 
<img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/FF_12ch_Margin.png" alt= “” width="40%" height="40%">
</p>



