# CMS Apollo Command Module Manual
<center>
<img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/Rev2CM_C+R.jpg" alt= "Charlie and Rui" width="40%" >
</center>

This page documents the Apollo Command Module used by CMS and built by Cornell University. The CM is used by the CMS Track Finder, the CMS Inner Tracker DTC, and the CMS BRIL systems.

## Quick-links

- [This Document's source](https://gitlab.com/apollo-lhc/cornell-cm/Manual)
- [Apollo SM Manual](https://apollo-lhc.gitlab.io/)
- [apollo-lhc github organization](https://github.com/apollo-lhc)
  - [CM FPGA FW](https://github.com/apollo-lhc/CM_FPGA_FW). This repo is the base firmware for both FPGAs on the CM. It has instances with and without the EMP framework. 
  - [MCU fw](https://github.com/apollo-lhc/cm_mcu). This includes a wiki with additional information about the MCU firmware and its uses.
  - [MCU SW tools](https://github.com/apollo-lhc/mcu_tools). Tools such as `sflash` and python scripts for loading the clock and the eeprom are located in this repository.
  - You can also find the hardware repositories for the CM here, including schematics, layout, BOMs, and constraint files for the FPGAs.
