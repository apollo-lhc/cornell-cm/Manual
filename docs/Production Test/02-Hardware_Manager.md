# Access and Program FPGA via Hardware Manager
<p style='text-align: right;'>
<em>Rui Zou, May 23 2023</em>
</p>

1. Log in `lnx4189.classe.cornell.edu`. Do `. /nfs/opt/Xilinx/Vivado/2020.2/settings64.sh`.
2. Open Vivado GUI in VNC viewer. Then open hardware manager.

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/OpenHW.png" alt= “” width="60%" height="60%">

3. Add virtual cable. Click on autoconnect if the option is grey. 

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/AutoConnect.png" alt= “” width="60%" height="60%">
    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/AddVirtualCable.png" alt= “” width="60%" height="60%">

    The hostname should be the apollo ethernet IP. Leave the rest to default values.
    
    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/AddVirtualCable1.png" alt= “” width="60%" height="60%">

4. You should see the FPGA(s) in the chain. 

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/FPGAsinHW.png" alt= “” width="40%" height="640%">

    If you do not see any FPGAs, check if the CM is powered on (BUTool) and jtag_sm is set to on (MCU CLI).

5. To program the FPGA(s), right click on the chip and select ("Program FPGA).

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/ProgramFPGA.png" alt= “” width="40%" height="640%">


## VNC Viewer (for Cornell users)
1. Make sure VNC viewer is installed on your PC. 
2. Log in `lnx4189.classe.cornell.edu`.
3. `vncserver -geometry 1920x1080 :56`, where 56 can be changed to any number. 
4. Log out and log back into `lnx4189` with: `ssh -L5956:localhost:5956 USERNAME@lnx4189.classe.cornell.edu`
5. Open VNC viewer on your PC. Connect to localhost:5956. Enter password to lnx4189 when prompted.
