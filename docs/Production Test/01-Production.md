# Overview
<p style='text-align: right;'>
<em>Rui Zou, Sep 12 2022</em>
</p>

This manual documents the production testing procedure for CMs at Cornell. 
Access to `lnx4189` is required.

[Checklist](https://docs.google.com/spreadsheets/d/13Gawl6bkomlKUtLDfTpmNeug6E5Fl6HG1fDbTjaFL-4/edit?usp=sharing)


