# C2C Test

<p style='text-align: right;'>
<em>Rui Zou, May 23 2023</em>
</p>

Simple test to test C2C links between FPGAs and the Zynq. Each FPGA has two C2C links. This test requires the CM to be mated to an SM.

## Firmware Location


## Procedure
1. log into zynq
2. `BUTool.exe -a`
3. check CM is powered up in `status`
4. program the firmware: `svfplayer PL_XVC.XVC_1`
5. unlock Axi: `unlockAXI`
6. check C2C links in `status`

## Evaluation
Correct status should display as following:

Report errors otherwise.

## Alternative Test
Complementary IBERT firmware can be run with a loopback connector to test the C2C links on the CM.
```
/mnt/scratch/rz393/firmware/
```

## Rev1 Legacy Files
Firmware for rev2 CM with rev1 SM with 2 C2C links/FPGA:
```
/mnt/scratch/rz393/firmware/top_Cornell_rev2_p1_VU13p-1-SM_7s_2C2C_v1.bit
/mnt/scratch/rz393/firmware/top_Cornell_rev2_p2_VU13p-1-SM_7s_v1.bit
```
Firmware for rev1 CM with rev1 SM wit 1 C2C link/FPGA:
```

```
