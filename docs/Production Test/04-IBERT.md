#IBERT Test Using Vivado GUI

<p style='text-align: right;'>
<em>Rui Zou, May 24 2023</em>
</p>

We use IBERT to test optical integrity. There are two components: a 4-day long PRBS test (target BER: 10^-16) and eyescan. In the lab we use PRBS-31 pattern with LPM mode and an insertion loss at Nyquist frequency set at 3dB. 

## Firmware and Software
We currently use the Vivado IBERT GUI. 
For IT-DTC-like configuration:
```
FPGA1: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p1_VU13p-1-SM_7s_IBERT_lpGBT_v1_25GLHS.bit
FPGA2: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p2_VU13p-1-SM_7s_IBERT_lpGBT_v1_25GLHS.bit
```
For OT-TF-like configuration:
```
FPGA1: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p1_VU13p-1-SM_7s_heaters_allQuads_25GRHS.bit
FPGA2: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p2_VU13p-1-SM_7s_heaters_allQuads_25GRHS.bit
```
The same firmware can be found on lxplus: `/afs/cern.ch/work/r/rzou/public/firmware/`
Both sets of firmware are compiled with a clock signal at 322.265625 MHz on refclk0, but 320 MHz works as well.

## Procedure
1. Log in `lnx4189.classe.cornell.edu` (or `imperialdaq002` at TIF). Set up Vivado environment. Open Vivado GUI in VNC viewer and connect to the responding apollo. (See [instructions on adding virtual cable and setting up VNC viewer](02-Hardware_Manager.md).) Make sure the laser on all fireflies are on by logging into the apolloIP.

        ssh cms@apolloIP
        minicom -D /dev/ttyUL1
        ff xmit on all

    To quit MCU CLI, do Ctrl+A, X.

2. Program the FPGA(s) with the right firmware. Check if the Quads are all locked.

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/QuadsLocked.png" alt= “” width="40%" height="40%">

    If the Quads are not locked, check if the corresponding SI chips are programmed with the right project. Power supply issues can also cause some Quads to appear unlocked.

3. Adding the links either manually, using auto-detect links or using the tcl script (). It's possible auto-detect does not detect all connected links. 

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/AutoDetect.png" alt= “” width="40%" height="40%">

    See below for how to add a link manually. Internal Loopback should be left unchecked to test the entire path including the optical engine and fiber cable (as default).

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/CreateLink.png" alt= “” width="100%" height="100%">

4. Make sure PRBS-31 is the selected pattern and DFE mode is off. 
5. Do resets in order (you can reset the entire link group at once): Reset Tx -> Reset Rx -> Reset BER.
6. You should see number of bits increment. If they appear stuck in the GUI, you should see the increment after right clicking and selecting to refresh the I/O objects.
7. For the PRBS test, one just need to wait for the desired BER to reach. The current goal for produciton is set to have the BER reach 1e-16 without any errors. See below for example of what we would like to see.

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/PRBS_rev2_12ch_1e-16.png" alt= “” width="80%" height="80%">

8. To take an eyescan, right click on the link and select `Create Scan`. Normally one should change the horizonal and vertical increment to 1 to get a smooth eye diagram. 

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/CreateScan.png" alt= “” width="100%" height="100%">

    To save the eyescan, right click on the eye and select `Write Scan Data`. This will save the eyescan data to a csv file. We can then plot it with the plotting script.

    <img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/SaveScan.png" alt= “” width="90%" height="90%">

    You can also call [the tcl script](https://github.com/apollo-lhc/Cornell_CM_Production_Scripts/blob/master/autotuning/tcl/apollo10_eyescan.tcl) to take and save eyescans on all links (may need some changes depending how the optical engines are cabled). 

9. If there are links that do not pass the PRBS test criteria or have bad eyes, one should see [next section](#procedure-to-run-optimization-on-link-xonfiguration) and run optimization on the link. Details on qualification of the eyescan and how different parameters affect the eye shapes can be found [here](../Link Test and Eyescans/02-Eyescans.md).

## Procedure to Run Optimization on Link Configuration

For links that do not have wide open eyes or do not reach 1e-16 BER with zero errors, an optimization scan on the link parameters is needed. To do that one needs to use the autotuning script. The script and some instructions on how to use it can be found [here](https://github.com/apollo-lhc/Cornell_CM_Production_Scripts#autotuning-system-for-xilinx-mgts).

## Procedure to Compile and Plot Eyescans

Once the scans are all saved, one can plot the csv files and compile them into one latex document. See the script and instructions [here](https://github.com/apollo-lhc/Cornell_CM_Production_Scripts#ibertpy-plotting-script). Example of the plotted pdf show below.

<img src="https://gitlab.com/apollo-lhc/cornell-cm/Manual/-/raw/master/images/Example_Eyescan.png" alt= “” width="50%" height="50%">

### Rev1 Legacy Files
```
KU15P: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p1_VU13p-1-SM_7s_heaters_v2_lpGBT.bit
VU7P: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p2_VU13p-1-SM_7s_heaters_v2_lpGBT.bit
```
