# Power and Thermal Test

<p style='text-align: right;'>
<em>Rui Zou, May 23 2023</em>
</p>

This tests the maximum power the blade can exert. 

Current thermal constraints are as following:

* max FPGA temperature: 80 C
* max FF temperature: 50 C
* max fan level: 10/15

Door on the crate should remain open when the test is conducted without a heat exchanger. Current procedure requires hardware manager. This test should only be done in a crate.

## Firmware Location
For IT-DTC-like configuraiton:
```
FPGA1: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p1_VU13p-1-SM_7s_heaters_v2_lpGBT.bit
FPGA2: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p2_VU13p-1-SM_7s_heaters_v2_lpGBT.bit
```
For OT-TF-like configuraiton:
```
FPGA1: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p1_VU13p-1-SM_7s_heaters_allQuads_25GRHS.bit
FPGA2: /nfs/cms/tracktrigger/rzou/firmware/top_Cornell_rev2_p2_VU13p-1-SM_7s_heaters_allQuads_25GRHS.bit
```
The same firmware can be found on lxplus: `/afs/cern.ch/work/r/rzou/public/firmware/`

## Procedure
1. Make sure CM power is on in Grafana ([TIF](https://cmstkph2-dps.web.cern.ch/cmstkph2-dps/grafana/d/YVW4b-n4k/rev2-cm-dashboard?orgId=1&var-WhichApollo=AP213), [Cornell](https://www.classe.cornell.edu/cms/grafana/psbATCA/d/YVW4b-n4k/rev2-cm-dashboard?orgId=2&refresh=30s&var-WhichApollo=AP205&from=now-1h&to=now), [BU](https://edf.bu.edu/shelf_data/d/F99hQrHVz/apollo-blade-sm?orgId=1&from=now-3h&to=now&var-Blade=Apollo212&refresh=1m)) by looking at FPGA temperatures (should be room temperature or above) and power from the 12 V (should be around 40 W or more).
    
    If not on, ssh into target apollo IP and power up the SM.

        ssh cms@apolloIP
        BUTool.exe -a
        cmpwrup
    
    If Grafana information is empty, ssh into target apollo IP and do
       
        ssh cms@apolloIP
        systemctl restart BUMonitor.service 
        
2. ssh into target apollo IP. Set alarm threshold to be higher on MCU. And enable the lasers on the fireflies.

        ssh cms@apolloIP
        minicom -D /dev/ttyUL1
        alm settemp ff 55
        alm settemp fpga 85  
        ff xmit on all

    To quit MC U CLI, do Ctrl+A X.

3. Set shelf manager threshold to higher values from lnx4189 (or pcuptrack001 at Tif) to avoid increasing fan level (this step currently does not work at Tif. Instead at TIF do in MCU CLI: `i2cs off status`)

        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM Regulator Max' unr 70
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM Regulator Max' ucr 65
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM Regulator Max' unc 60
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM Firefly Max T' unr 56
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM Firefly Max T' ucr 55
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM Firefly Max T' unc 54
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM MCU Temperatu' unr 56
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM MCU Temperatu' ucr 55
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM MCU Temperatu' unc 50
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM FPGA1 Tempera' unr 86
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM FPGA1 Tempera' ucr 85
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM FPGA1 Tempera' unc 84
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM FPGA2 Tempera' unr 86
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM FPGA2 Tempera' ucr 85
        ipmitool -H 192.168.21.140 -P "" -t 0x96 sensor thresh 'CM FPGA2 Tempera' unc 84

    `192.168.21.140` should be the shelf manager IP. 

4. Open hardware manager. Program the FPGAs via hardware manager

5. Wait for the temperature to reach thermal equilibrium on Grafana. Then try enabling 2 (`0x3 = 0b11`) sets of heaters by typing in hardware manager:

        create_hw_axi_txn -force enable1 [get_hw_axis hw_axi_1] -type write -address 0x8100_0140 -len 1 -data 00000001
        run_hw_axi [get_hw_axi_txns enable1]
        create_hw_axi_txn -force adjust1 [get_hw_axis hw_axi_1] -type write -address 0x8100_0144 -len 1 -data 00000003
        run_hw_axi [get_hw_axi_txns adjust1]

    For FPGA2:

        create_hw_axi_txn -force enable2 [get_hw_axis hw_axi_2] -type write -address 0x8300_0140 -len 1 -data 00000001
        run_hw_axi [get_hw_axi_txns enable2]
        create_hw_axi_txn -force adjust2 [get_hw_axis hw_axi_2] -type write -address 0x8300_0144 -len 1 -data 00000003
        run_hw_axi [get_hw_axi_txns adjust2]
    
6. Try changing the number of heaters to 4 sets on FPGA1 (`0b1111 = 0xf`)

        create_hw_axi_txn -force adjust1 [get_hw_axis hw_axi_1] -type write -address 0x8100_0144 -len 1 -data 0000000f
        run_hw_axi [get_hw_axi_txns adjust1]

    Or FPGA2:

        create_hw_axi_txn -force adjust2 [get_hw_axis hw_axi_2] -type write -address 0x8300_0144 -len 1 -data 0000000f
        run_hw_axi [get_hw_axi_txns adjust2]


7. Keep increasing heaters until both FPGAs reach 80 C or fireflies reach 50 C

8. Save a snapshot of grafana for documentaiton purposes if needed. Gradually decrease the number of heaters to 0


## Rev1 Legacy Files
For rev1 CM:
```
FPGA1: 
>JTAG-AXI address:
FPGA2: 
>JTAG-AXI address:
```

