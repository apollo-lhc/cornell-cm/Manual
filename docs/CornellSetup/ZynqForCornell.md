# Setup for Apollo Blades at Cornell

Special settings required to run the SM in the Cornell ATCA shelf. 

## Settings for dnf/yum

* The ATCA shelf is not connected to the internet. To install packages, you can use a proxy server. The proxy server address is

        http://cache01.classe.cornell.edu:3128

    This can be set in the `/etc/dnf/dnf.conf` file. 

        proxy=http://cache01.classe.cornell.edu:3128

    and also added to your bashrc for pip etc via `export http_proxy=http://cache01.classe.cornell.edu:3128` and `export https_proxy=http://cache01.classe.cornell.edu:3128`.

* the grafana server address needs to be updated. The server is on lnx4189; currently this is located at 192.168.21.1. Also in the past the server was configured differently than the one at the TIF; however, since the alma9 update, the server is down.

## Changes needed on lnx4189 (server for the shelf)

The linux host lnx4189 serves as a dhcp server, grafana/graphite server, name server and a limited proxy to the internet. The firewall must allow ntp and dhcp traffic.

* lnx4189 must be set up to forward ip4 traffic from the shelf subnet to the wider lab internet.

        lnx4189:/etc >sudo egrep -r ip_forward .
        sysctl.d/net.ipv4.ip_forward.enable.conf:net.ipv4.ip_forward = 1

* The dhcp server (package dhcp-server) is configured to give out addresses in the range 192.168.21.0/24. For each host the MAC address is used to assign a fixed IP address. The MAC addresses are stored in the file `/etc/dhcp/dhcpd.conf`. The file `/etc/dhcp/dhcpd.leases` contains the current leases. The dhcp server is configured as follows:

        lnx4189:/etc >sudo cat dhcp/dhcpd.conf
        #
        # DHCP Server Configuration file

        default-lease-time 3600;
        max-lease-time 7200;

        subnet 192.168.21.0 netmask 255.255.255.0 {
            include "/etc/dhcp-cms/dhcpd.conf";
        }


        lnx4189:/var/log >cat /etc/dhcp-cms/dhcpd.conf

        # NB: everything in this file is inside a

        # subnet 192.168.21.0 netmask 255.255.255.0 {}

        range 192.168.21.100 192.168.21.110;
        range 192.168.21.140 192.168.21.160;
        option routers 192.168.21.1;
        option ntp-servers 128.84.46.26;
        option domain-name-servers 192.168.21.1;

        option domain-name "classe.cornell.edu";

        host apollo210 {
            hardware ethernet 00:50:51:FF:22:08;
            option host-name "apollo210";
            fixed-address apollo210;
        }
        host apollo210-icmp {
            hardware ethernet 00:80:E1:84:87:74;
            fixed-address apollo210-ipmc;
        }

    This is not the full file, but shows an example. The "option host-name" tells the blade what its hostname is. The "fixed-address" is the IP address that the blade will be assigned. The "hardware ethernet" is the MAC address of the blade. The "option routers" is the default gateway for the blade. The "option ntp-servers" is the NTP server that the blade will use. The "option domain-name-servers" is the DNS server that the blade will use. The "option domain-name" is the domain name that the blade will use.

* The host names are assigned statically in `/etc/hosts`. Current assignment as of 7/2024:

        lnx4189:~ >cat /etc/hosts
        127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
        ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

        # apollo rev2

        192.168.21.150 apollo200
        192.168.21.151 apollo201
        192.168.21.152 apollo202
        192.168.21.153 apollo203
        192.168.21.154 apollo204
        192.168.21.155 apollo205
        192.168.21.156 apollo206
        192.168.21.157 apollo207
        192.168.21.158 apollo208
        192.168.21.159 apollo209
        192.168.21.160 apollo210

        # apollo rev2 ipmc

        192.168.21.100 apollo200-ipmc
        192.168.21.101 apollo201-ipmc
        192.168.21.102 apollo202-ipmc
        192.168.21.103 apollo203-ipmc
        192.168.21.104 apollo204-ipmc
        192.168.21.105 apollo205-ipmc
        192.168.21.106 apollo206-ipmc
        192.168.21.107 apollo207-ipmc
        192.168.21.108 apollo208-ipmc
        192.168.21.109 apollo209-ipmc
        192.168.21.110 apollo210-ipmc

        # random

        192.168.21.115 piatcamon
        192.168.21.116 eltek
    These hosts are also assigned in the dhcpd.conf file.

* The linux host acts as a name server (package bind). We only look at ipv4 addresses.
    Change /etc/named.conf as follows:

        options {
            listen-on port 53 { 192.168.21.1;  };
            //listen-on-v6 port 53 { ::1; };
                allow-query     { localhost; 192.168.21.0/24; };

        }

    Add `OPTIONS="-4"` to /etc/sysconfig/named.

Without the name server and the ip4 forwarding, things like ntp will break and the date/time will be wrong/random.
